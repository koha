# German translations for Koha package.
# Copyright (C) 2020 THE Koha'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Koha package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-06 19:52-0300\n"
"PO-Revision-Date: 2020-05-12 15:04+0000\n"
"Last-Translator: Katrin Fischer <katrin.fischer@bsz-bw.de>\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1589295880.753348\n"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:236
msgid "Loading... you may continue scanning."
msgstr "Lädt... Sie können das Einlesen fortsetzen."

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:926
msgid "Double click to edit"
msgstr "Doppelklicken zum Bearbeiten"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:958
msgid "Delete"
msgstr "Löschen"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:961
msgid "Resolve"
msgstr "Beilegen"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:966
msgid "Actions"
msgstr "Aktionen"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:969
msgid "Edit notes"
msgstr "Notizen bearbeiten"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:1011
msgid "Update"
msgstr "Aktualisieren"

#: koha-tmpl/intranet-tmpl/prog/js/checkouts.js:1012
msgid "Cancel"
msgstr "Abbrechen"

#: koha-tmpl/intranet-tmpl/prog/js/letter.js:4
#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:20
msgid "Loading..."
msgstr "Ladevorgang …"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:10
msgid "First"
msgstr "Erste"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:11
msgid "Last"
msgstr "Letzte"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:12
msgid "Next"
msgstr "Nächste"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:13
msgid "Previous"
msgstr "Vorherige"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:15
msgid "No data available in table"
msgstr "Keine Daten verfügbar"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:16
msgid "Showing _START_ to _END_ of _TOTAL_ entries"
msgstr "Zeige _START_ bis _END_ von _TOTAL_ Einträgen"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:17
msgid "No entries to show"
msgstr "Keine Einträge"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:18
msgid "(filtered from _MAX_ total entries)"
msgstr "(Auswahl von _MAX_ Einträgen)"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:19
msgid "Show _MENU_ entries"
msgstr "Zeige _MENU_ Einträge"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:21
msgid "Processing..."
msgstr "Lädt..."

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:22
msgid "Search:"
msgstr "Suche:"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:23
msgid "No matching records found"
msgstr "Keine übereinstimmenden Datensätze gefunden"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:25
msgid "Copy to clipboard"
msgstr "In Zwischenablage kopieren"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:26
msgid ""
"Press <i>ctrl</i> or <i>⌘</i> + <i>C</i> to copy the table data<br>to your "
"system clipboard.<br><br>To cancel, click this message or press escape."
msgstr ""
"Drücken Sie <i>Strg</i> oder <i>⌘</i> + <i>C</i> um die Daten aus der "
"Tabelle<br>in Ihre Zwischenablage zu kopieren.<br><br>Um Abzubrechen, "
"klicken Sie diese Nachricht an oder drücken Sie Escape."

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:28
#, javascript-format
msgid "Copied %d rows to clipboard"
msgstr "%d Zeilen in Zwischenablage kopiert"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:29
msgid "Copied one row to clipboard"
msgstr "Eine Zeile in Zwischenablage kopiert"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:37
#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:39
msgid "Clear filter"
msgstr "Filter leeren"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:51
msgid "All"
msgstr "Alle"

#: koha-tmpl/intranet-tmpl/prog/js/datatables.js:358
msgid "a an the"
msgstr "a an the der die das ein eine einer einem einen eines"

#: koha-tmpl/intranet-tmpl/prog/js/table_filters.js:26
msgid "Deactivate filters"
msgstr "Filter deaktivieren"

#: koha-tmpl/intranet-tmpl/prog/js/table_filters.js:44
msgid "Activate filters"
msgstr "Filter aktivieren"

#: koha-tmpl/intranet-tmpl/prog/js/subscription-add.js:58
msgid "The vendor does not exist"
msgstr "Der Lieferant existiert nicht"

#: koha-tmpl/intranet-tmpl/prog/js/pages/results.js:317
#: koha-tmpl/intranet-tmpl/prog/js/pages/results.js:319
msgid "No cover image available"
msgstr "Kein Coverbild verfügbar"

#: koha-tmpl/intranet-tmpl/prog/js/pages/results.js:341
#: koha-tmpl/intranet-tmpl/prog/js/pages/results.js:352
msgid "You must select at least one record"
msgstr "Sie müssen mindestens einen Datensatz auswählen"

#: koha-tmpl/intranet-tmpl/prog/js/pages/results.js:363
msgid "At least two records must be selected for merging"
msgstr "Sie müssen mindestns zwei zu verschmelzende Datensätze auswählen"
